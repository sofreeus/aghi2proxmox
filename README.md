![PROXMOX](assets/WIP-meetup-posting-image-1200x675.png)

ProxMox Virtual Environment
===========================

A Gentle, Hands-on Intro...
- Give intro into selves. 
- Why Prox? - what got you into it?


Setup
- Add Nodes
- Create Users/Perms - Class will hand each other accounts
- Add OS install media

Create a VM

Create LXC Container

Backups
- Backup a VM
- Restore a VM
- Configure routine backups

Clusters
- Create a cluster
- Move a guest

About PMox:
- Free Software (like OpenStack, WordPress, and everything Red Hat makes)
- Support options from about 80 euros per year
- Compares to VMware ESXi and vSphere

See also:
- https://www.youtube.com/watch?v=bsyNb4MNy_0
- https://www.youtube.com/watch?v=4COPbV-2-Qs
- https://www.youtube.com/watch?v=0YyMTg9qc74
- https://pve.proxmox.com/pve-docs/pve-admin-guide.pdf
- https://pve.proxmox.com/wiki/Linux_Container
